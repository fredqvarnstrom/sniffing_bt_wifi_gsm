# -*- coding: utf-8 -*-
from kivy.uix.button import Button
from kivy.uix.listview import ListView
from kivy.uix.floatlayout import FloatLayout
from kivy.clock import Clock
from kivy.adapters.listadapter import ListAdapter
from kivy.adapters.models import SelectableDataItem

from kivy.uix.listview import ListItemButton

import random


class DataItem(SelectableDataItem):
	def __init__(self, name, txt, **kwargs):
		self.name = name
		self.text = txt
		super(DataItem, self).__init__(**kwargs)


class MainView(FloatLayout):
	"""
    Implementation of a ListView using the kv language.
    """

	def __init__(self, **kwargs):
		super(MainView, self).__init__(**kwargs)
		data_items = [DataItem('One', 'txt_1'), DataItem('Two', 'txt_2'), DataItem('Three', 'txt_3')]
		list_item_args_converter = lambda row_index, obj: {'text': obj.name, 'size_hint_y': None, 'height': 25}
		self.list_adapter = ListAdapter(data=data_items, args_converter=list_item_args_converter,
										selection_mode='single', propagate_selection_to_data=False,
										allow_empty_selection=False, cls=ListItemButton)

		self.list_view = ListView(adapter=self.list_adapter, pos=(100, 100), size_hint=(0.5, 0.2))
		self.add_widget(self.list_view)
		btn = Button(text='asdfg', on_release=self.ttt, size_hint=(0.2, 0.5))
		self.add_widget(btn)
		self.toggle = 'adding'
		Clock.schedule_interval(self.update_list_data, 5)

	def update_list_data(self, dt):
		items = self.list_adapter.data
		if self.toggle == 'adding':
			item = DataItem('New ' * random.randint(1, 2), 'aaa')
			items.append(item)
			self.toggle = 'changing'
			print('added ' + item.name)
		else:
			random_index = random.randint(0, len(items) - 1)
			item = items[random_index]
			items[random_index] = DataItem('Changed', 'sadg')
			self.toggle = 'adding'
			print('changed {0} to {1}'.format(item.name, items[random_index].name))

	def ttt(self, *args):
		print self.list_adapter.selection


if __name__ == '__main__':
	from kivy.base import runTouchApp
	runTouchApp(MainView(width=800))
