from Queue import Queue
import os

import pcapy
import struct
import traceback
import time
import xml.etree.ElementTree
import threading
import sqlite3 as lite


class WiFiCtrl(Queue, threading.Thread):

    cap = None
    _wifi_queue = None
    conf_file_name = ''

    _stop_event = threading.Event()
    _terminate_event = threading.Event()

    dedicated = None

    curs = None
    conn = None

    def __init__(self):
        threading.Thread.__init__(self)
        self._wifi_queue = Queue()
        self.debug = 1

        if self.debug > 0:
            self.conn = lite.connect('data/devices.sqlite')
            self.conf_file_name = 'data/config.xml'
        else:
            self.conn = lite.connect('/home/pi/pos_diamond/data/devices.sqlite')
            self.conf_file_name = '/home/pi/pos_diamond/data/config.xml'

        self.curs = self.conn.cursor()
        self.check_db()

    def initialize(self):
        os.system("iw dev wlan1 interface add mon0 type monitor")
        time.sleep(1)
        os.system("ifconfig mon0 up")
        time.sleep(1)
        try:
            self.cap = pcapy.open_live('mon0', 1514, 1, 0)
            return True
        except pcapy.PcapError as e:
            return False

    def pause_monitoring(self):
        os.system("ifconfig mon0 down")
        time.sleep(1)
        os.system("iw dev mon0 del")
        time.sleep(1)
        return True

    def terminate(self):
        self._terminate_event.set()

    def run(self):
        self._terminate_event.clear()
        while True:
            if self._terminate_event.isSet():
                break
            if not self._stop_event.isSet():
                (header, pkt) = self.cap.next()
                if self.cap.datalink() == 0x7F:
                    self.packet_handler(pkt)
            else:
                time.sleep(1)

    @property
    def wifi_queue(self):
        return self._wifi_queue

    def packet_handler(self, pkt):
        rtlen = struct.unpack('h', pkt[2:4])[0]
        ftype = (ord(pkt[rtlen]) >> 2) & 3
        stype = ord(pkt[rtlen]) >> 4
        # check if probe request
        if ftype == 0 and stype == 4:
            rtap = pkt[:rtlen]
            frame = pkt[rtlen:]
            # parse bssid
            bssid = frame[10:16].encode('hex')
            bssid = ':'.join([bssid[x:x + 2] for x in xrange(0, len(bssid), 2)])
            mac = str(bssid)
            mac = mac.upper()
            # parse rssi
            rssi = struct.unpack("b", rtap[-4:-3])[0]
            # parse essid
            essid = ''
            if len(frame) > 25:
                if ord(frame[25]) > 0:
                    essid = frame[26:26+ord(frame[25])]
                    b_my_client = False
                else:
                    b_my_client = True
            else:
                b_my_client = False

            if b_my_client and rssi != 0:
                data = (mac, rssi)
                if self.dedicated is None or mac == self.dedicated:
                    print "Client found. MAC: ", mac, "ESSID: ", essid, "   Signal Strength: ", rssi
                    self._wifi_queue.put(data)

    def set_param_to_xml(self, tag_name, new_val):
        et = xml.etree.ElementTree.parse(self.conf_file_name)
        for child_of_root in et.getroot():
            if child_of_root.tag == tag_name:
                child_of_root.text = new_val
                et.write(self.conf_file_name)
                return True
        return False

    def get_param_from_xml(self, param):
        """
        Get configuration parameters from the config.xml
        :param param: parameter name
        :return: if not exists, return None
        """
        root = xml.etree.ElementTree.parse(self.conf_file_name).getroot()
        tmp = None
        for child_of_root in root:
            if child_of_root.tag == param:
                tmp = child_of_root.text
                break
        return tmp

    def stop(self):
        self._stop_event.set()

    def resume(self):
        self._stop_event.clear()

    def stopped(self):
        return self._stop_event.isSet()

    def set_dedicated(self, mac):
        self.dedicated = mac

    def check_db(self):
        """
        Check sqlite db file and create if not exists
            Column :  No. mac, dev_type, A, n,
        :return:
        """
        try:
            sql = 'create table if not exists tb_dev ' \
                  '(' \
                  'id INTEGER PRIMARY KEY AUTOINCREMENT, ' \
                  'mac TEXT, ' \
                  'dev_type TEXT, ' \
                  'val_A NUMERIC, ' \
                  'val_n NUMERIC' \
                  ');'
            self.curs.execute(sql)
            self.conn.commit()
            return True

        except lite.Error as e:
            print("Error %s:" % e.args[0])
            return False

    def get_dev(self, addr):
        """
        Get A-value from the db
        :param addr: Mac address of the device
        :return:
        """
        try:
            sql = "SELECT * from tb_dev WHERE mac='" + addr + "';"
            self.curs.execute(sql)
            row = self.curs.fetchone()
            return row
        except lite.Error as e:
            print "Error %s:" % e.args[0]
            return None
if __name__ == "__main__":

    ctrl = WiFiCtrl()

    if ctrl.initialize():
        print "Monitoring is started..."
    else:
        print "Failed to start monitoring..."

    ctrl.run()

