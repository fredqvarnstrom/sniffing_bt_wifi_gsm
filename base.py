import xml.etree.ElementTree
import threading
import sqlite3 as lite


class Base(threading.Thread):

    _version = 1.0
    conf_file_name = ''

    _stop_event = threading.Event()
    _terminate_event = threading.Event()

    dedicated = None

    curs = None
    conn = None

    @property
    def version(self):
        """
        get
        :return:
        """
        return self._version

    @version.setter
    def version(self, value):
        self._version = value

    def __init__(self):
        threading.Thread.__init__(self)
        self.debug = 1

        if self.debug > 0:
            self.conn = lite.connect('data/devices.sqlite')
            self.conf_file_name = 'data/config.xml'
        else:
            self.conn = lite.connect('/home/pi/pos_diamond/data/devices.sqlite')
            self.conf_file_name = '/home/pi/pos_diamond/data/config.xml'

        self.curs = self.conn.cursor()
        self.check_db()

    def set_param_to_xml(self, tag_name, new_val):
        et = xml.etree.ElementTree.parse(self.conf_file_name)
        for child_of_root in et.getroot():
            if child_of_root.tag == tag_name:
                child_of_root.text = new_val
                et.write(self.conf_file_name)
                return True
        return False

    def get_param_from_xml(self, param):
        """
        Get configuration parameters from the config.xml
        :param param: parameter name
        :return: if not exists, return None
        """
        root = xml.etree.ElementTree.parse(self.conf_file_name).getroot()
        tmp = None
        for child_of_root in root:
            if child_of_root.tag == param:
                tmp = child_of_root.text
                break
        return tmp

    def stop(self):
        self._stop_event.set()

    def terminate(self):
        self._terminate_event.set()

    def resume(self):
        self._stop_event.clear()

    def stopped(self):
        return self._stop_event.isSet()

    def set_dedicated(self, mac):
        self.dedicated = mac

    def check_db(self):
        """
        Check sqlite db file and create if not exists
            Column :  No. mac, dev_type, A, n,
        :return:
        """
        try:
            sql = 'create table if not exists tb_dev ' \
                  '(' \
                  'id INTEGER PRIMARY KEY AUTOINCREMENT, ' \
                  'mac TEXT, ' \
                  'dev_type TEXT, ' \
                  'val_A NUMERIC, ' \
                  'val_n NUMERIC' \
                  ');'
            self.curs.execute(sql)
            self.conn.commit()
            return True

        except lite.Error as e:
            print("Error %s:" % e.args[0])
            return False

    def get_dev(self, addr, dev_type):
        """
        Get A-value from the db
        :param addr: Mac address of the device
        :param dev_type: device type bt or wifi
        :return:
        """
        try:
            sql = "SELECT * from tb_dev WHERE mac='" + addr + "' and dev_type='" + dev_type + "';"
            self.curs.execute(sql)
            row = self.curs.fetchone()
            return row
        except lite.Error as e:
            print "Error %s:" % e.args[0]
            return None

if __name__ == '__main__':
    inst_irrig = Base()
    print(inst_irrig)
